var demo, loaded=false;

function startupDraw () {
	var label = document.getElementById("labelClick");
	
	try {
		var canvas = document.getElementById("glCanvas");
		var gl = initGL(canvas);
		demo = new Demo();
		resetTimer();	
		demo.initialize(gl, "textureCanvas");
	}
	catch(err) {
		
		return;
	}
	
	loaded = true;
	resetTimer();	
	requestFrame(demo, gl);				
}

function requestFrame(demo, gl) {
	demo.render(gl);
	last = performance.now();
	window.requestAnimationFrame(function () { 
		requestFrame(demo, gl); 
	});
}

function initGL(canvas) {
	try {
		gl = canvas.getContext("experimental-webgl", { alpha: true, depth: true });
		return gl;
	}
	catch (e) {
		console.log("Error in WebGL initialization.");
	}
	if (!gl) {
		alert("Could not initialize WebGL");
		return null;
	}
}
